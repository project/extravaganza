********************************************************************************
                      D R U P A L    M O D U L E
********************************************************************************
Name: PM Vista
Author: D34dMan <shibinkidd -a@t- gmail -d.t- com>
Drupal: 7
Date: 28th April 2013.

********************************************************************************
WHAT IS EXTRAVAGANZA?

Extravaganza provides few blocks and content that could be used to enhance user
experience.  It focuses on enhancing the experience of Project Management Drupal
module. Extravaganza has a good synergy with PM Vista theme which provides css
support for its blocks.

Blocks provided by extravaganza are,

  Extravaganza: Date and Time
  Provide a date and time display of when the page was requested. The date and
  time formats can be chosen separately in the block admin. In addition to other
  formats, Extravaganza adds two more defaults formats to the system so that its
  selected by default for the module when it is enabled.

  Extravaganza: User Profile
  Provides a user profile block that contains username, user picture if present,
  a logout link and an account edit link.

********************************************************************************
RESOURCES

Projects
PM Vista Theme            : http://drupal.org/project/pm_vista
Project Management Module : http://drupal.org/project/pm


********************************************************************************
INSTALLATION:

0.  Download and place 'extravaganza' directory into your Drupal modules
    directory.

2.  Enable the module extravaganza. 

3.  After enabling extravaganza the blocks are made available for placing and 
    configuration in:

     Administer > Structure > Blocks

********************************************************************************
