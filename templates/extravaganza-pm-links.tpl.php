<?php
/**
 * @file
 * Theme template for user profile block which shows user name, profile pic,
 * logout link and link to account edit if accessible.
 */

?>
<div id="extravaganza-pm-links" class="clearfix">
  <ul class="extravaganza-dashboard-links">
  <?php foreach($links as $link): ?>
    <li class="<?php echo implode(' ', $link['class']); ?>">
      <div class="extravaganza-pm-links-wrapper">
        <?php echo $link['view'] ?>
        <?php echo $link['add'] ?>
      </div>
    </li>
  <?php endforeach; ?>
  </ul>
</div>
