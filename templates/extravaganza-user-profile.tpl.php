<?php
/**
 * @file
 * Theme template for user profile block which shows user name, profile pic,
 * logout link and link to account edit if accessible.
 */

?>
<div id="user-profile-badge" class="clearfix">
  <?php echo $image; ?>
  <div class="name ">
    <span class="name-welcome"><?php  echo $name_prefix; ?></span>
    <span class="name-title"><?php  echo $name; ?></span>
  </div>
  <div class="logout"><?php echo $menu['logout']; ?></div>
  <div class="account"><?php echo $menu['account']; ?></div>
</div>
