<?php
/**
 * @file
 * Theme template for clock block which shows date and time.
 */

?>
<div id="site-clock" class="clearfix">
  <div id="site-clock-wrapper">
    <time class="date" datetime="<?php echo date('Y-m-d', $date); ?>"><?php echo $formated_date; ?></time>
    <time class="time"><?php echo $formated_time; ?></time>
  </div>
</div>
